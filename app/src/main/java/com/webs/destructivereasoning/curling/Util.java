package com.webs.destructivereasoning.curling;

import android.graphics.Color;

/**
 * Created by harwiltz on 12/13/16.
 */

public class Util {

    //GENERAL DIMENSIONS
    public static int WIDTH;
    public static int HEIGHT;
    public static float ANNULUS_WIDTH;
    public static float ROCK_RADIUS;
    public static float RELATIVE_WIDTH = 0.8f;
    public static float HOUSE_RADIUS;
    public static float HOUSE_CENTER_X;
    public static float HOUSE_CENTER_Y;
    public static float SHEET_LENGTH;

    //ICE LINES
    public static final float CENTER_LINE_WEIGHT = 0.003f;
    public static final float HOGLINE_WEIGHT = 0.01f;
    public static final int LINE_COLOR = Color.rgb(85,85,85);
    public static final int HOGLINE_COLOR = Color.rgb(100,0,0);

    //RELATIVE SIZES OF COUNTER GRAPHICS
    public static final float COUNTER_POS = 0.12f;
    public static final float COUNTER_RADIUS = 0.25f;

    //FRICTION VALUES
    public static final float MAX_FRICTION = 0.012f;
    public static final float MIN_FRICTION = 0.3f * MAX_FRICTION;
    public static final float MAX_SWEEP_VELOCITY = 80.0f;

    //SIDEBAR VALUES
    public static final float SIDEBAR_WIDTH = 0.05f;

    //NEXT SHOT BOX
    public static final float NEXT_SHOT_BOX_HEIGHT = 0.25f;

    //CURL VALUES AND GRAPHICS
    public static final float SLIDER_LENGTH = 0.85f;
    public static final float SLIDER_BACKGROUND = 0.08f;
    public static final float MAX_OMEGA = 5.0f;
    public static final float OMEGA_TO_CURL = 0.40f; //Changed to sqrt relationship

    //MESSAGE BOX
    public static final float MESSAGE_BOX_WIDTH = 0.8f;
    public static final float MESSAGE_BOX_HEIGHT = 0.3f;

    //TRANSITION TIMES
    public static final long HOUSE_TRANSITION_TIME = 5000; //milliseconds

    //SWEEPING STUFF
    public static final float SWEEP_PARTICLE = 0.007f;
    public static final int PARTICLE_BASECOLOR = Color.rgb(0x00,0xdd,0xdd);
    public static final int PARTICLE_LIFESPAN = 10;
    public static final int PARTICLE_DENSITY = 20;

    //GAME VALUES
    public static int NUM_ENDS = 4;
    public static String PLAYER1_NAME = "Player 1";
    public static String PLAYER2_NAME = "Player 2";
    public static final float SCORESHEET_HEIGHT = 0.3f;
    public static int PLAYER1_COLOR = Color.rgb(0x00,0x00,0xFF);
    public static int PLAYER2_COLOR = Color.rgb(0xFF,0x00,0x00);

    public static float euclideanDistance(Vector2f u, Vector2f v) {
        return (float)Math.sqrt((u.getX() - v.getX())*(u.getX()-v.getX()) + (u.getY() - v.getY())*(u.getY()-v.getY()));
    }
}
