package com.webs.destructivereasoning.curling;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by harwiltz on 12/31/16.
 */

public class Particle {

    private Vector2f position;
    private Vector2f velocity;
    private int baseColor;
    private int lifeSpan;
    private int age;

    public Particle(float x, float y, Vector2f velocity, int baseColor, int lifeSpan) {
        this.position = new Vector2f(x,y);
        this.baseColor = baseColor;
        this.lifeSpan = lifeSpan;
        this.age = 0;
        this.velocity = velocity;
    }

    public void update() {
        this.age++;
        this.position = Vector2f.add(this.position,this.velocity);
    }

    public void render(Canvas canvas, Paint paint, float yOffset) {
        paint.setColor(baseColor);
        paint.setAlpha((int)(255.0f * (float)(lifeSpan - age)/lifeSpan));
        canvas.drawCircle(position.getX(),position.getY() - yOffset,Util.WIDTH * Util.SWEEP_PARTICLE,paint);
    }

    public boolean dead() {
        return age >= lifeSpan;
    }

}
