package com.webs.destructivereasoning.curling;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class MainMenuView extends SurfaceView implements Runnable{

    public static final int PERIOD = 17; //17ms refresh rate

    private static final int MAX_ROCKS = 10;
    private static final float MAX_VEL = 5.0f;

    private static final float PLAY_BUTTON_HEIGHT = 0.12f;
    private static final float SETTINGS_BUTTON_HEIGHT = 0.12f;
    private static final float BUTTON_HEIGHT = PLAY_BUTTON_HEIGHT + SETTINGS_BUTTON_HEIGHT;

    private int width;
    private int height;

    private float playTop;
    private float playBottom;
    private float settingsTop;
    private float settingsBottom;

    private Thread actionThread;
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder holder;

    private Random bobandy;

    private boolean playing;

    private enum MenuItem {Play,Settings,None};

    private MenuItem item;

    private Rock rocks[];

    private Context context;
    private Activity activity;
    private Intent intent;

    public MainMenuView(Context context, Activity activity, int width, int height) {
        super(context);
        this.activity = activity;
        this.context = context;
        this.width = width;
        this.height = height;

        Util.WIDTH = width;
        Util.HEIGHT = height;
        Util.HOUSE_RADIUS = Util.RELATIVE_WIDTH * width / 2;
        Util.ANNULUS_WIDTH = (float)(Util.HOUSE_RADIUS) * 1.25f/4.0f;
        Util.HOUSE_CENTER_X = width/2;
        Util.ROCK_RADIUS = Util.ANNULUS_WIDTH / 3.2f;
        Rock.ROCK_RADIUS = Util.ROCK_RADIUS;
        Rock.COLOR_RADIUS = Util.ROCK_RADIUS * 0.6f;
        Util.SHEET_LENGTH = 24.3f * Util.HOUSE_RADIUS - height;
        Util.HOUSE_CENTER_Y = height/2;

        actionThread = null;
        holder = getHolder();
        paint = new Paint();

        bobandy = new Random();

        this.playTop = height * (1.0f - BUTTON_HEIGHT);
        this.playBottom = height * (1.0f - SETTINGS_BUTTON_HEIGHT);
        this.settingsTop = playBottom;
        this.settingsBottom = height;

        this.item = MenuItem.None;

        rocks = new Rock[MAX_ROCKS];

        for(int c = 0; c < MAX_ROCKS; c++) {
            rocks[c] = new Rock(Color.rgb(bobandy.nextInt(0xFF),bobandy.nextInt(0xFF),bobandy.nextInt(0xFF)),false);
            rocks[c].setPosition(new Vector2f(width * bobandy.nextFloat(),height * bobandy.nextFloat()));
            rocks[c].setVelocity(
                    Vector2f.mul(
                            bobandy.nextFloat() * MAX_VEL,
                            new Vector2f((bobandy.nextInt(2) - 1) * bobandy.nextFloat(),(bobandy.nextInt(2) - 1) * bobandy.nextFloat()).normalized()));
            rocks[c].setInitialOmega(rocks[c].getSpeed());
            rocks[c].setFriction(0);
        }
    }

    @Override
    public void run() {
        while(playing) {
            update();
            render();
            control();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                if(event.getY() >= playTop && event.getY() <= playBottom) {
                    this.item = MenuItem.Play;
                }
                else if(event.getY() >= settingsTop && event.getY() <= settingsBottom) {
                    this.item = MenuItem.Settings;
                }
                else {
                    this.item = MenuItem.None;
                }
                break;
            case MotionEvent.ACTION_UP:
                this.item = MenuItem.None;
                if(event.getY() >= playTop && event.getY() <= playBottom) {
                    intent = new Intent(context,MainActivity.class);
                    activity.startActivity(intent);
                }
                else if(event.getY() >= settingsTop && event.getY() <= settingsBottom) {
                    intent = new Intent(context,SettingsActivity.class);
                    activity.startActivity(intent);
                }
                break;
            default:
                break;
        }
        return true;
    }

    public void update() {
        for(int c = 0; c < MAX_ROCKS; c++) {
            rocks[c].update(false);
            float x,y;
            x = rocks[c].getPosition().getX();
            y = rocks[c].getPosition().getY();
            if(x < -Util.ROCK_RADIUS || x > width + Util.ROCK_RADIUS || y < -Util.ROCK_RADIUS || y > height + Util.ROCK_RADIUS) {
                float xmul,ymul;
                if(x < 0) xmul = 1.0f;
                else if (x > width) xmul = -1.0f;
                else xmul = bobandy.nextInt(2) - 1;
                if(y < 0) ymul = 1.0f;
                else if (y > height) ymul = -1.0f;
                else ymul = bobandy.nextInt(2) - 1;
                rocks[c].setVelocity(
                        Vector2f.mul(
                                bobandy.nextFloat() * MAX_VEL,
                                new Vector2f(xmul * bobandy.nextFloat(),ymul * bobandy.nextFloat()).normalized()));
                rocks[c].setInitialOmega(rocks[c].getSpeed());
            }
        }
        checkCollisions();
    }

    public void render() {
        if(holder.getSurface().isValid()) {
            canvas = holder.lockCanvas();

            canvas.drawColor(Color.argb(255,250,250,255));
            paint.setStyle(Paint.Style.FILL_AND_STROKE);

            //paint.setAlpha(150);
            drawHouse();

            //Draw Scene
            for(int c = 0; c < MAX_ROCKS; c++) {
                rocks[c].render(canvas,paint,0);
            }

            paint.setColor(Color.argb(100,250,250,255));
            canvas.drawRect(0,0,width,height,paint);

            //Draw Buttons
            paint.setColor(Color.argb(180,0x44,0x44,0xaa));
            if(this.item == MenuItem.Play) paint.setAlpha(225);
            else paint.setAlpha(180);
            canvas.drawRect(0,playTop,width,playBottom,paint);
            paint.setColor(Color.argb(180,0xaa,0x44,0x44));
            if(this.item == MenuItem.Settings) paint.setAlpha(225);
            else paint.setAlpha(180);
            canvas.drawRect(0,settingsTop,width,height,paint);

            float fontSize = PLAY_BUTTON_HEIGHT * 0.4f * height;

            paint.setColor(Color.WHITE);
            paint.setAlpha(225);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(fontSize);

            canvas.drawText("Play",(float)width/2,(playTop + playBottom)/2 + fontSize/2,paint);
            canvas.drawText("Settings",(float)width/2,(settingsTop + height)/2 + fontSize/2,paint);

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public void checkCollisions() {
        for(int c = 0; c < MAX_ROCKS; c++) {
            for(int j = c + 1; j < MAX_ROCKS; j++) {
                Vector2f r = Vector2f.sub(rocks[c].getPosition(),rocks[j].getPosition());
                if(r.getMagnitude() <= 2 * Util.ROCK_RADIUS) {
                    Vector2f momentum = Vector2f.add(rocks[c].getVelocity(),rocks[j].getVelocity());
                    float proj = Vector2f.dot(momentum,r) / Vector2f.dot(r,r);
                    rocks[c].setVelocity(Vector2f.mul(proj,r));
                    rocks[j].setVelocity(Vector2f.sub(momentum,Vector2f.mul(proj,r)));
                }
            }
        }
    }

    public void drawHouse() {
        paint.setColor(Color.BLUE);
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y,Util.HOUSE_RADIUS,paint);
        paint.setColor(Color.rgb(250,250,255));
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y,Util.HOUSE_RADIUS - Util.ANNULUS_WIDTH,paint);
        paint.setColor(Color.rgb(255,0,0));
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y,Util.HOUSE_RADIUS - 2*Util.ANNULUS_WIDTH,paint);
        paint.setColor(Color.rgb(250,250,255));
        canvas.drawCircle(width/2,Util.HOUSE_CENTER_Y,Util.ROCK_RADIUS,paint);
    }

    public void control() {
        try {
            actionThread.sleep(PERIOD);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        playing = true;
        actionThread = new Thread(this);
        actionThread.start();
    }

    public void pause() {
        playing = false;
        try {
            actionThread.join();
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

}