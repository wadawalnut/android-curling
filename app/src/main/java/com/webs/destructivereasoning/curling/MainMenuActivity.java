package com.webs.destructivereasoning.curling;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainMenuActivity extends Activity {
    private MainMenuView menuView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        //setContentView(R.layout.mainmenu);
        menuView = new MainMenuView(this,this,size.x,size.y);
        setContentView(menuView);
    }

    @Override
    public void onResume() {
        super.onResume();
        menuView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        menuView.pause();
    }

    public void processClick(View v) {
        Intent intent;
        switch(v.getId()) {
            case R.id.play:
                intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.settings:
                intent = new Intent(this,SettingsActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}