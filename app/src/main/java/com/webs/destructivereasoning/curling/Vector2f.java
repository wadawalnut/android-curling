package com.webs.destructivereasoning.curling;

/**
 * Created by harwiltz on 12/13/16.
 */

public class Vector2f {

    private float x;
    private float y;

    public static Vector2f ZERO = new Vector2f(0,0);

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Vector2f normalized() {
        float length = (float)Math.sqrt(this.x * this.x + this.y * this.y);
        if(length == 0.0f) return this;
        return new Vector2f(this.x/length,this.y/length);
    }

    public static Vector2f add(Vector2f u, Vector2f v) {
        return new Vector2f(u.getX() + v.getX(), u.getY() + v.getY());
    }

    public static Vector2f addVectors(Vector2f[] vectors) {
        Vector2f r = Vector2f.ZERO;
        for(int c = 0; c < vectors.length; c++) {
            r = Vector2f.add(r,vectors[c]);
        }
        return r;
    }

    public static Vector2f sub(Vector2f u, Vector2f v) {
        return new Vector2f(u.getX() - v.getX(), u.getY() - v.getY());
    }

    public static Vector2f mul(float k, Vector2f u) {
        return new Vector2f(k * u.getX(), k * u.getY());
    }

    public static float dot(Vector2f u, Vector2f v) {
        return u.getX() * v.getX() + u.getY() * v.getY();
    }

    public float getMagnitude() {
        return (float)Math.sqrt(this.x * this.x + this.y * this.y);
    }
}
