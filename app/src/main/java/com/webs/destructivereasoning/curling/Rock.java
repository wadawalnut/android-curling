package com.webs.destructivereasoning.curling;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by harwiltz on 12/13/16.
 */

public class Rock {

    public static Rock rocks[] = new Rock[16];
    public static int rockCount = 0;

    public static float ROCK_RADIUS;
    public static float COLOR_RADIUS;

    private static int BG = Color.rgb(64,64,64);

    private static final float HANDLE_WIDTH = 0.2f;
    private static final float HANDLE_HEIGHT = 0.6f;

    private Vector2f pos;
    private Vector2f vel;
    private Vector2f accel;

    private float theta;
    private float omega;

    private float initialSpeed;
    private float initialOmega;

    private float friction;

    private boolean ob;

    public enum SPIN {CCW, CW};

    private SPIN spin;
    private int color;

    private RectF handleRect;

    private int opacity;

    public Rock(int color) {
        this.pos = new Vector2f(Util.WIDTH/2,Util.HEIGHT - ROCK_RADIUS * 2);
        this.vel = new Vector2f(0,0f);
        this.accel = Vector2f.ZERO;
        this.friction = Util.MAX_FRICTION;
        this.color = color;
        this.spin = SPIN.CCW;
        rocks[Rock.rockCount++] = this;
        //if(rockCount > 16) rockCount = 0;
        ob = false;
        opacity = 255;
        theta = 0.0f;
        initialSpeed = initialOmega = 0.0f;
    }

    public Rock(int color, boolean addToArray) {
        this.pos = new Vector2f(Util.WIDTH/2,Util.HEIGHT - ROCK_RADIUS * 2);
        this.vel = new Vector2f(0,0f);
        this.accel = Vector2f.ZERO;
        this.friction = Util.MAX_FRICTION;
        this.color = color;
        this.spin = SPIN.CCW;
        if(addToArray) rocks[Rock.rockCount++] = this;
        //if(rockCount > 16) rockCount = 0;
        ob = false;
        opacity = 255;
        theta = 0.0f;
        initialSpeed = initialOmega = 0.0f;
    }

    public void update() {
        if(this.getSpeed() != 0.0f && !ob) System.out.println("Friction: " + this.friction * 100 / Util.MAX_FRICTION + "%");
        if(initialSpeed != 0.0f) omega = initialOmega * (float)Math.sqrt(this.getSpeed() / initialSpeed); //Changed to sqrt relationship
        theta += omega;
        if(this.getPosition().getX() < 0.0f || this.getPosition().getX() > Util.WIDTH) this.flagOB();
        else if(this.getPosition().getY() < Util.HOUSE_CENTER_Y - Util.HOUSE_RADIUS - Util.HOUSE_RADIUS/3) this.flagOB();
        if(ob) return;

        Vector2f f = Vector2f.mul(-this.friction,this.vel.normalized());
        Vector2f curlDir = new Vector2f(this.vel.getY(),0).normalized();
        Vector2f curl = Vector2f.mul((-omega/Util.MAX_OMEGA) * f.getMagnitude() * Util.OMEGA_TO_CURL,curlDir);
        //this.vel = Vector2f.add(this.vel,Vector2f.add(this.accel,f));
        this.vel = Vector2f.addVectors(new Vector2f[]{vel,accel,f,curl});
        if(this.vel.getY() > 0.0f) this.vel.setY(0.0f);
        if(this.vel.getMagnitude() < 0.1f) this.vel = Vector2f.ZERO;
        this.pos = Vector2f.add(this.pos,this.vel);
    }

    public void update(boolean obCheck) {
        if(this.getSpeed() != 0.0f && !ob) System.out.println("Friction: " + this.friction * 100 / Util.MAX_FRICTION + "%");
        if(initialSpeed != 0.0f) omega = initialOmega * (float)Math.sqrt(this.getSpeed() / initialSpeed); //Changed to sqrt relationship
        theta += omega;
        if(obCheck) {
            if (this.getPosition().getX() < 0.0f || this.getPosition().getX() > Util.WIDTH)
                this.flagOB();
            else if (this.getPosition().getY() < Util.HOUSE_CENTER_Y - Util.HOUSE_RADIUS - Util.HOUSE_RADIUS / 3)
                this.flagOB();
            if(ob) return;
        }

        Vector2f f = Vector2f.mul(-this.friction,this.vel.normalized());
        Vector2f curlDir = new Vector2f(this.vel.getY(),0).normalized();
        Vector2f curl = Vector2f.mul((-omega/Util.MAX_OMEGA) * f.getMagnitude() * Util.OMEGA_TO_CURL,curlDir);
        //this.vel = Vector2f.add(this.vel,Vector2f.add(this.accel,f));
        this.vel = Vector2f.addVectors(new Vector2f[]{vel,accel,f,curl});
        if(obCheck) if(this.vel.getY() > 0.0f) this.vel.setY(0.0f);
        if(obCheck) if(this.vel.getMagnitude() < 0.1f) this.vel = Vector2f.ZERO;
        this.pos = Vector2f.add(this.pos,this.vel);
    }


    public void render(Canvas canvas, Paint paint, float yOffset) {
        if(ob) return;
        if(opacity != 255) paint.setColor(Color.argb(opacity,Color.red(BG),Color.green(BG),Color.blue(BG)));
        else paint.setColor(BG);
        canvas.translate(this.pos.getX(),this.pos.getY() - yOffset);
        canvas.rotate(theta);
        canvas.drawCircle(0.0f,0.0f,ROCK_RADIUS,paint);
        if(opacity != 255) paint.setColor(Color.argb(opacity,Color.red(color),Color.green(color),Color.blue(color)));
        else paint.setColor(color);
        canvas.drawCircle(0.0f, 0.0f,COLOR_RADIUS,paint);
        if(opacity != 255) paint.setColor(Color.argb(opacity,Color.red(BG),Color.green(BG),Color.blue(BG)));
        else paint.setColor(BG);
        handleRect = new RectF(
                 - ROCK_RADIUS * HANDLE_WIDTH/2,
                 - ROCK_RADIUS,
                 + ROCK_RADIUS * HANDLE_WIDTH/2,
                 - ROCK_RADIUS + 2 * ROCK_RADIUS * HANDLE_HEIGHT);
        canvas.drawRoundRect(handleRect,ROCK_RADIUS/10,ROCK_RADIUS/10,paint);
        canvas.rotate(-theta);
        canvas.translate(-this.pos.getX(),-this.pos.getY() + yOffset);
        /*
        canvas.drawCircle(pos.getX(), pos.getY() - yOffset,ROCK_RADIUS,paint);
        if(opacity != 255) paint.setColor(Color.argb(opacity,Color.red(color),Color.green(color),Color.blue(color)));
        else paint.setColor(color);
        canvas.drawCircle(pos.getX(), pos.getY() - yOffset,COLOR_RADIUS,paint);
        if(opacity != 255) paint.setColor(Color.argb(opacity,Color.red(BG),Color.green(BG),Color.blue(BG)));
        else paint.setColor(BG);
        handleRect = new RectF(
                pos.getX() - ROCK_RADIUS * HANDLE_WIDTH/2,
                pos.getY() - ROCK_RADIUS - yOffset,
                pos.getX() + ROCK_RADIUS * HANDLE_WIDTH/2,
                pos.getY() - yOffset - ROCK_RADIUS + 2 * ROCK_RADIUS * HANDLE_HEIGHT);
        canvas.drawRoundRect(handleRect,ROCK_RADIUS/10,ROCK_RADIUS/10,paint);
        */
    }

    public Vector2f getPosition() {
        return pos;
    }

    public Vector2f getVelocity() { return vel; }

    public float getSpeed() { return vel.getMagnitude(); }

    public void setAcceleration(Vector2f accel) {
        this.accel = accel;
    }

    public void setVelocity(Vector2f vel) {
        this.vel = vel;
    }

    public void setPosition(Vector2f pos) { this.pos = pos; }

    public boolean isOutOfBounds() { return ob; }

    public void flagOB() { this.ob = true; }

    public int getColor() { return this.color; }

    public void setOpacity(int opacity) { this.opacity = opacity; }

    public void setInitialOmega(float omega) { this.omega = omega; this.initialOmega = omega; }

    public void setInitialSpeed(float speed) { this.initialSpeed = speed; }

    public void setFriction(float friction) { this.friction = friction; }
}
