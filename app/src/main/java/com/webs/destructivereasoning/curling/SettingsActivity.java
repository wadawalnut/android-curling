package com.webs.destructivereasoning.curling;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        setContentView(R.layout.settings);
        Spinner spinner = (Spinner)findViewById(R.id.endspinner);
        EditText player1Name = (EditText)findViewById(R.id.player1name);
        EditText player2Name = (EditText)findViewById(R.id.player2name);
        EditText player1Color = (EditText)findViewById(R.id.color1text);
        EditText player2Color = (EditText)findViewById(R.id.color2text);
        List<Integer> items = new ArrayList<>();
        for(int c = 1; c <= 10; c++) {
            items.add(c);
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Choose amount of ends to play");
        spinner.setSelection(Util.NUM_ENDS - 1);
        if(!Util.PLAYER1_NAME.equals("Player 1")) player1Name.setText(Util.PLAYER1_NAME);
        if(!Util.PLAYER2_NAME.equals("Player 2")) player2Name.setText(Util.PLAYER2_NAME);
        String colorString = Integer.toHexString(Util.PLAYER1_COLOR & 0x00FFFFFF);
        while(colorString.length() < 6) colorString = "0" + colorString;
        player1Color.setText("#" + colorString);
        settingsClick(findViewById(R.id.preview1));
        colorString = Integer.toHexString(Util.PLAYER2_COLOR & 0x00FFFFFF);
        while(colorString.length() < 6) colorString = "0" + colorString;
        player2Color.setText("#" + colorString);
        settingsClick(findViewById(R.id.preview2));
    }

    public void settingsClick(View v) {
        String colorString = "";
        String nameString = "";
        String errorMessage = "";
        EditText textField;
        TextView colorPreview;
        TextView errorText = (TextView)findViewById(R.id.settingserror);
        Spinner endSpinner = (Spinner)findViewById(R.id.endspinner);
        int color = Color.YELLOW;
        switch(v.getId()) {
            case R.id.save:
                /*
                intent = new Intent(this,MainMenuActivity.class);
                startActivity(intent);
                */
                textField = (EditText)findViewById(R.id.color1text);

                colorString = textField.getText().toString();

                if(!isValidHexColor(colorString) && colorString.length() != 0) {
                    textField.setTextColor(Color.RED);
                    errorMessage = "Invalid color entry. Color must be in format #rrggbb, where rrggbb is a hexadecimal number. Try again.";
                    errorText.setText(errorMessage);
                    break;
                }

                textField.setTextColor(Color.BLACK);

                if(colorString.length() != 0) {
                    colorString = colorString.substring(1);

                    color = Integer.parseInt(colorString, 16);
                    Util.PLAYER1_COLOR = Color.rgb((color & 0xFF0000) >> 16, (color & 0x00FF00) >> 8, color & 0x0000FF);
                }

                textField = (EditText)findViewById(R.id.color2text);

                colorString = textField.getText().toString();

                if(!isValidHexColor(colorString) && colorString.length() != 0) {
                    textField.setTextColor(Color.RED);
                    errorMessage = "Invalid color entry. Color must be in format #rrggbb, where rrggbb is a hexadecimal number. Try again.";
                    errorText.setText(errorMessage);
                    break;
                }

                textField.setTextColor(Color.BLACK);

                if(colorString.length() != 0) {
                    colorString = colorString.substring(1);

                    color = Integer.parseInt(colorString, 16);
                    Util.PLAYER2_COLOR = Color.rgb((color & 0xFF0000) >> 16, (color & 0x00FF00) >> 8, color & 0x0000FF);
                }

                if(Util.PLAYER1_COLOR == Util.PLAYER2_COLOR) {
                    textField = (EditText)findViewById(R.id.color1text);
                    textField.setTextColor(Color.RED);
                    textField = (EditText)findViewById(R.id.color2text);
                    textField.setTextColor(Color.RED);
                    errorMessage = "Error: both player colors are the same! Please change one of the colors.";
                    errorText.setText(errorMessage);
                    break;
                }

                textField = (EditText)findViewById(R.id.player1name);
                nameString = textField.getText().toString();
                if(nameString.length() > 0) Util.PLAYER1_NAME = nameString;

                textField = (EditText)findViewById(R.id.player2name);
                nameString = textField.getText().toString();
                if(nameString.length() > 0) Util.PLAYER2_NAME = nameString;

                Util.NUM_ENDS = endSpinner.getSelectedItemPosition() + 1;

                this.finish();
                break;
            case R.id.cancel:
                /*
                intent = new Intent(this,MainMenuActivity.class);
                startActivity(intent);
                */
                this.finish();
                break;
            case R.id.preview1:
                textField = (EditText)findViewById(R.id.color1text);

                colorString = textField.getText().toString();
                System.out.println("Player 1 color: " + colorString);

                if(!isValidHexColor(colorString)) {
                    textField.setText("");
                    break;
                }

                textField.setTextColor(Color.BLACK);

                colorString = colorString.substring(1);

                color = Integer.parseInt(colorString,16);
                color = Color.rgb((color & 0xFF0000) >> 16,(color & 0x00FF00) >> 8, color & 0x0000FF);

                colorPreview = (TextView)findViewById(R.id.color1);
                colorPreview.setBackgroundColor(color);
                break;
            case R.id.preview2:
                textField = (EditText)findViewById(R.id.color2text);

                colorString = textField.getText().toString();

                if(!isValidHexColor(colorString)) {
                    textField.setText("");
                    break;
                }

                textField.setTextColor(Color.BLACK);

                colorString = colorString.substring(1);

                color = Integer.parseInt(colorString,16);
                color = Color.rgb((color & 0xFF0000) >> 16,(color & 0x00FF00) >> 8, color & 0x0000FF);

                colorPreview = (TextView)findViewById(R.id.color2);
                colorPreview.setBackgroundColor(color);
                break;
            default:
                break;
        }
    }

    private boolean isValidHexColor(String string) {
        if(string.length() != 7 || string.charAt(0) != '#') return false;
        for(int c = 0; c < string.length(); c++) {
            if(!(string.charAt(c) > 47 && string.charAt(c) < 58) && !(string.charAt(c) > 64 && string.charAt(c) < 91) && !(string.charAt(c) > 96 || string.charAt(c) < 123)) return false;
        }
        return true;
    }
}