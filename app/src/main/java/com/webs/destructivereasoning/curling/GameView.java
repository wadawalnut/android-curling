package com.webs.destructivereasoning.curling;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Random;


public class GameView extends SurfaceView implements Runnable {

    /**
     * TODO
     * Move End textbox to bottom of screen
	 * Collision reaction as projection of v on distance    [DONE - needs improvement]
     * Fix sidebar hog line positions   					[Good enough]
     * Get out of sweep when rock is OB 					[DONE]
	 * Switch to Sweep at hog line							[Not necessary right now, worse for debugging]
     */

    public static final int PERIOD = 17; //17ms refresh rate

    private static LinkedList<Particle> particles = new LinkedList<Particle>();

    volatile boolean playing;
    Thread gameThread;

    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder holder;

    private final int WIDTH;
    private final int HEIGHT;

    float speed;
    boolean isTouchDown;

    float yOffset;

    private boolean canSlide;
    private boolean houseReady;

    Vector2f startPosition = Vector2f.ZERO;
    Vector2f endPosition = Vector2f.ZERO;
    Vector2f lastPosition = Vector2f.ZERO;
    Vector2f currentPosition = Vector2f.ZERO;
    float lastTime;

    long houseStartTime;

    private enum PlayState {Shooting,Sweeping,House,End};

    private PlayState state;

    private int color1,color2;

    private String endMessage;
    private int points;

    private boolean showNextShotBox;

    private int[] colors;
    private String[] playerNames;

    private float sliderPos;

    private float sweepVelocity;
    private Vector2f lastSweepPos;
    private long lastSweepTime;

    Object particleMutex;

    Random bobandy;

    int[][] scores;
    int currentEnd;
    int lastEnd;

    short hammer;

    public GameView (Context context, int width, int height) {
        super(context);
        state = PlayState.House;
        houseStartTime = System.currentTimeMillis();
        WIDTH = width;
        HEIGHT = height;
        Util.WIDTH = width;
        Util.HEIGHT = height;
        Util.HOUSE_RADIUS = Util.RELATIVE_WIDTH * WIDTH / 2;
        Util.ANNULUS_WIDTH = (float)(Util.HOUSE_RADIUS) * 1.25f/4.0f;
        Util.HOUSE_CENTER_X = WIDTH/2;;
        Util.ROCK_RADIUS = Util.ANNULUS_WIDTH / 3.2f;
        Rock.ROCK_RADIUS = Util.ROCK_RADIUS;
        Rock.COLOR_RADIUS = Util.ROCK_RADIUS * 0.6f;
        Util.SHEET_LENGTH = 24.3f * Util.HOUSE_RADIUS - HEIGHT;
        Util.HOUSE_CENTER_Y = -(Util.SHEET_LENGTH - 2 * Util.HOUSE_RADIUS);
        gameThread = null;
        playing = true;
        holder = getHolder();
        paint = new Paint();
        speed = 2.0f;
        isTouchDown = false;
        yOffset = -(Util.SHEET_LENGTH - 2 * Util.HOUSE_RADIUS) - HEIGHT/2;
        canSlide = true;
        houseReady = false;
        lastTime = 0.0f;
        Random rand = new Random();
        color1 = Util.PLAYER2_COLOR;
        color2 = Util.PLAYER1_COLOR;
        points = 0;
        endMessage = "";
        showNextShotBox = true;
        colors = new int [] {
                Color.argb(180,Color.red(color1),Color.green(color1),Color.blue(color1)),
                Color.argb(180,Color.red(color2),Color.green(color2),Color.blue(color2))};
        playerNames = new String[] {Util.PLAYER1_NAME, Util.PLAYER2_NAME};
        sweepVelocity = 0.0f;
        lastSweepPos = Vector2f.ZERO;
        particleMutex = new Object();
        bobandy = new Random();
        scores = new int[2][Util.NUM_ENDS];
        currentEnd = 0;
        hammer = 0;
    }

    @Override
    public void run() {
        //yOffset = HOUSE_CENTER_Y;
        while(playing) {
            update();
            render();
            control();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float speed;
        switch(event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                if(state == PlayState.Sweeping) {
                    synchronized (particleMutex) {
                        for (int c = 0; c < Util.PARTICLE_DENSITY; c++) {
                            particles.add(new Particle(event.getX(), event.getY() + yOffset, new Vector2f(bobandy.nextFloat() * 8.0f, bobandy.nextFloat() * 3.0f), Util.PARTICLE_BASECOLOR, Util.PARTICLE_LIFESPAN));
                            lastSweepPos = new Vector2f(event.getX(),event.getY() + yOffset);
                        }
                    }
                }
                if(!canSlide) break;
                if(event.getY() < HEIGHT * Util.NEXT_SHOT_BOX_HEIGHT) {
                    sliderPos = event.getX();
                    if(sliderPos < WIDTH * (1.0f - Util.SLIDER_LENGTH))
                        sliderPos = WIDTH * (1.0f - Util.SLIDER_LENGTH);
                    else if(sliderPos > WIDTH * Util.SLIDER_LENGTH)
                        sliderPos = WIDTH * Util.SLIDER_LENGTH;
                    sliderPos -= WIDTH / 2;
                    break;
                }
                startPosition = new Vector2f(event.getX(),event.getY());
                currentPosition = new Vector2f(event.getX(),event.getY());
                lastPosition = new Vector2f(event.getX(),event.getY());
                lastTime = System.currentTimeMillis();
                isTouchDown = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if(state == PlayState.Sweeping) {
                    Vector2f currentPos = new Vector2f(event.getX(), event.getY() + yOffset);
                    sweepVelocity = Math.abs(Vector2f.sub(currentPos,lastSweepPos).getX());
                    lastSweepPos = currentPos;
                    if(sweepVelocity > Util.MAX_SWEEP_VELOCITY) sweepVelocity = Util.MAX_SWEEP_VELOCITY;
                    synchronized(particleMutex) {
                        for(int c = 0; c < Util.PARTICLE_DENSITY; c++) {
                            particles.add(new Particle(event.getX(), event.getY() + yOffset, new Vector2f(bobandy.nextFloat() * 8.0f, bobandy.nextFloat() * 3.0f), Util.PARTICLE_BASECOLOR, Util.PARTICLE_LIFESPAN));
                        }
                    }
                }
                if(!canSlide) break;
                if(event.getY() < HEIGHT * Util.NEXT_SHOT_BOX_HEIGHT) {
                    sliderPos = event.getX();
                    if(sliderPos < WIDTH * (1.0f - Util.SLIDER_LENGTH))
                        sliderPos = WIDTH * (1.0f - Util.SLIDER_LENGTH);
                    else if(sliderPos > WIDTH * Util.SLIDER_LENGTH)
                        sliderPos = WIDTH * Util.SLIDER_LENGTH;
                    sliderPos -= WIDTH / 2;
                    break;
                }
                lastPosition.setX(currentPosition.getX());
                lastPosition.setY(currentPosition.getY());
                currentPosition = new Vector2f(event.getX(),event.getY());
                break;
            case MotionEvent.ACTION_UP:
                //if(!canSlide) break;
                endPosition = new Vector2f(event.getX(),event.getY());
                if(state == PlayState.Shooting) {
                    if(Rock.rocks[Rock.rockCount - 1].getSpeed() > 0.1f) {
                        canSlide = false;
                        Rock.rocks[Rock.rockCount-1].setInitialOmega((sliderPos) * 2 * Util.MAX_OMEGA / WIDTH);
                    }
                }
                else if(state == PlayState.House) {
                    if(event.getY() < HEIGHT * (1.0f - Util.NEXT_SHOT_BOX_HEIGHT)) break;
                    showNextShotBox = false;
                    houseReady = true;
                    houseStartTime = System.currentTimeMillis();
                }
                else if(state == PlayState.End) {
                    Rock.rockCount = 0;
                    for(int c = 0; c < 16; c++) Rock.rocks[c] = null;
                    houseReady = true;
                    houseStartTime = System.currentTimeMillis();
                    currentEnd++;
                    state = PlayState.House;
                }
                isTouchDown = false;
                break;
            default:
                break;
        }
        return true;
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void update() {
        for(int c = 0; c < Rock.rockCount; c++) {
            Rock.rocks[c].update();
        }

        float rockPos;

        switch(state) {
            case Shooting: //User swipes to shoot
                if (isTouchDown) {
                    if (!canSlide) return;
                    Vector2f dP = Vector2f.sub(currentPosition, lastPosition);
                    Vector2f a = Vector2f.mul(0.025f, dP);
                    Rock.rocks[Rock.rockCount - 1].setAcceleration(a);
                } else {
                    if(canSlide) break; //User hasn't swiped yet
                    Rock.rocks[Rock.rockCount - 1].setAcceleration(Vector2f.ZERO);
                    Rock.rocks[Rock.rockCount - 1].setInitialSpeed(Rock.rocks[Rock.rockCount-1].getSpeed());
                    particles.clear();
                    state = PlayState.Sweeping;
                }
                rockPos = Rock.rocks[Rock.rockCount-1].getPosition().getY();
                if(rockPos > Util.HEIGHT/2) yOffset = 0.0f;
                else if(rockPos <= -(Util.SHEET_LENGTH - 2*Util.HOUSE_RADIUS))
                    yOffset = -(Util.SHEET_LENGTH - 2*Util.HOUSE_RADIUS) - HEIGHT/2;
                else yOffset = rockPos - Util.HEIGHT/2;
                break;
            case Sweeping:
                rockPos = Rock.rocks[Rock.rockCount-1].getPosition().getY();
                if(lastSweepPos.getY() < rockPos && rockPos - lastSweepPos.getY() < 2 * Util.ROCK_RADIUS) {
                    //Rock.rocks[Rock.rockCount-1].setFriction((Util.MAX_SWEEP_VELOCITY + Util.MIN_FRICTION - sweepVelocity) * Util.MAX_FRICTION /(Util.MAX_SWEEP_VELOCITY + Util.MIN_FRICTION));
                    Rock.rocks[Rock.rockCount-1].setFriction(((Util.MAX_SWEEP_VELOCITY - sweepVelocity)/Util.MAX_SWEEP_VELOCITY)*(Util.MAX_FRICTION - Util.MIN_FRICTION) + Util.MIN_FRICTION);
                }
                else {
                    Rock.rocks[Rock.rockCount-1].setFriction(Util.MAX_FRICTION);
                }
                synchronized(particleMutex) {
                    ListIterator<Particle> iterator = particles.listIterator();
                    while (iterator.hasNext()) {
                        Particle p = iterator.next();
                        if (p.dead()) iterator.remove();
                        else p.update();
                    }
                }

                checkCollisions();

                Rock r = Rock.rocks[Rock.rockCount-1];

                if(!noMotion()) {
                    if(r.getVelocity().getMagnitude() == 0) {
                        if(yOffset > -(Util.SHEET_LENGTH - 2*Util.HOUSE_RADIUS - HEIGHT/5) - HEIGHT/2)
                            yOffset -= 15.0f;
                        break;
                    }
                    if(rockPos > Util.HEIGHT/2) yOffset = 0.0f;
                    else if(rockPos <= -(Util.SHEET_LENGTH - 2*Util.HOUSE_RADIUS - HEIGHT/5))
                        yOffset = -(Util.SHEET_LENGTH - 2*Util.HOUSE_RADIUS - HEIGHT/5) - HEIGHT/2;
                    else yOffset = rockPos - Util.HEIGHT/2;
                    break;
                }

                if(yOffset > -(Util.SHEET_LENGTH - 2 * Util.HOUSE_RADIUS - HEIGHT/5) - HEIGHT/2) {
                    yOffset -= 40.0f;
                    break;
                }
                if(r.getPosition().getY() > Util.HOUSE_CENTER_Y + 3.5 * Util.HOUSE_RADIUS + WIDTH*Util.HOGLINE_WEIGHT/2) r.flagOB();
                state = PlayState.House;
                houseReady = false;
                showNextShotBox = true;
                particles.clear();
                break;
            case House:
                if(Rock.rockCount == 16) {
                    showNextShotBox = false;
                    state = PlayState.End;
                    points = checkWinner();
                    break;
                }
                if(!houseReady) break;
                if(yOffset < 0.0f && System.currentTimeMillis() - houseStartTime < Util.HOUSE_TRANSITION_TIME) {
					//Animation from rock stop position back to the hack
                    if(System.currentTimeMillis() - houseStartTime < 100) yOffset += (System.currentTimeMillis() - houseStartTime+1);
                    else if(System.currentTimeMillis() - houseStartTime > Util.HOUSE_TRANSITION_TIME - 100)
                        yOffset += (Util.HOUSE_TRANSITION_TIME - (System.currentTimeMillis() - houseStartTime));
                    else yOffset += 100;
                }
                else {
                    yOffset = 0.0f;

                    new Rock(colors[(Rock.rockCount + 1) % 2]);
                    state = PlayState.Shooting;
                    canSlide = true;
                }
                break;
            case End:
                if(points == 0) {
                    endMessage = "Blank End";
                    scores[0][currentEnd] = scores[1][currentEnd] = 0;
                }
                else if(points > 0) { //First Shooter wins - hammer doesn't change
                    if(hammer == 0) {
                        endMessage = Util.PLAYER1_NAME + " scores " + points + " points.";
                        scores[0][currentEnd] = points;
                        scores[1][currentEnd] = 0;
                    }
                    else {
                        endMessage = Util.PLAYER2_NAME + " scores " + points + " points.";
                        scores[1][currentEnd] = points;
                        scores[0][currentEnd] = 0;
                    }
                }
                else { //Second Shooter wins - hammer changes
                    points = -points;
                    if(hammer == 0) {
                        endMessage = Util.PLAYER2_NAME + " scores " + points + " points.";
                        scores[1][currentEnd] = points;
                        scores[0][currentEnd] = 0;
                    }
                    else {
                        endMessage = Util.PLAYER1_NAME + " scores " + points + " points.";
                        scores[0][currentEnd] = points;
                        scores[1][currentEnd] = 0;
                    }

                    String tmpName = playerNames[0];
                    int tmpColor = colors[0];

                    playerNames[0] = playerNames[1];
                    playerNames[1] = tmpName;
                    colors[0] = colors[1];
                    colors[1] = tmpColor;

                    hammer = (short)((hammer + 1) % 2); //Alternates between 0 and 1
                }

                while(state == PlayState.End) try { render(); Thread.sleep(500);} catch (Exception ex) {}

                break;
        }
    }

    public void checkCollisions() {
        for(int c = 0; c < Rock.rockCount; c++) {
            if(Rock.rocks[c].isOutOfBounds()) continue;
            for(int j = c + 1; j < Rock.rockCount; j++) {
                if(Rock.rocks[j].isOutOfBounds()) continue;
                Vector2f r = Vector2f.sub(Rock.rocks[c].getPosition(),Rock.rocks[j].getPosition());
                if(r.getMagnitude() <= 2 * Util.ROCK_RADIUS) {
                    Vector2f momentum = Vector2f.add(Rock.rocks[c].getVelocity(),Rock.rocks[j].getVelocity());
                    float proj = Vector2f.dot(momentum,r) / Vector2f.dot(r,r);
                    Rock.rocks[c].setVelocity(Vector2f.mul(proj,r));
                    Rock.rocks[j].setVelocity(Vector2f.sub(momentum,Vector2f.mul(proj,r)));
                }
            }
        }
    }

    public synchronized void render() {
        if(holder.getSurface().isValid()) {
            //float rockPos = Rock.rocks[Rock.rockCount-1].getPosition().getY();
            canvas = holder.lockCanvas();

            //Draw ice
            canvas.drawColor(Color.argb(255,250,250,255));
            //Configure paint
            paint.setStyle(Paint.Style.FILL_AND_STROKE);

            drawSidebar();

            if(yOffset < -(Util.SHEET_LENGTH - 6 * Util.HOUSE_RADIUS)) drawHouse();

            //Draw three vertical lines through center (independent of yOffset)
            paint.setColor(Util.LINE_COLOR);
            canvas.drawRect(Util.WIDTH/2 - Util.CENTER_LINE_WEIGHT * WIDTH/2,0,Util.WIDTH/2 + Util.CENTER_LINE_WEIGHT * WIDTH/2, HEIGHT,paint);
            canvas.drawLine(Util.WIDTH/2 - Util.ROCK_RADIUS,0,Util.WIDTH/2 - Util.ROCK_RADIUS,HEIGHT,paint);
            canvas.drawLine(Util.WIDTH/2 + Util.ROCK_RADIUS,0,Util.WIDTH/2 + Util.ROCK_RADIUS,HEIGHT,paint);

            drawHogLines();
            if(state == PlayState.Shooting) {
                drawSlider();
            }

            synchronized(particleMutex) {
                ListIterator<Particle> iterator = particles.listIterator();
                while (iterator.hasNext()) {
                    iterator.next().render(canvas, paint, yOffset);
                }
            }

            for(int c = 0; c < Rock.rockCount; c++) {
                Rock.rocks[c].render(canvas,paint,yOffset);
            }

            drawCounters();

            if(state == PlayState.House && showNextShotBox) {
                paint.setColor(colors[(Rock.rockCount + 1) % 2]);
                canvas.drawRect(0,HEIGHT * (1.0f - Util.NEXT_SHOT_BOX_HEIGHT),WIDTH,HEIGHT,paint);

                paint.setColor(Color.WHITE);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize(128);
                canvas.drawText(playerNames[Rock.rockCount % 2] + "'s Shot",Util.WIDTH/2,HEIGHT * (1.0f - Util.NEXT_SHOT_BOX_HEIGHT/2),paint);
            }

            if(state == PlayState.End) {
                paint.setColor(Color.argb(180,80,80,180));
                canvas.drawRect(
                        Util.WIDTH*(1-Util.MESSAGE_BOX_WIDTH)/2,
                        Util.HEIGHT*(1-Util.MESSAGE_BOX_HEIGHT)/2,
                        Util.WIDTH*(1+Util.MESSAGE_BOX_WIDTH)/2,
                        Util.HEIGHT*(1+Util.MESSAGE_BOX_HEIGHT)/2,paint);

                paint.setColor(Color.WHITE);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize(80);
                canvas.drawText(endMessage,Util.WIDTH/2,Util.HEIGHT/2,paint);
                drawScoreSheet();
            }

            holder.unlockCanvasAndPost(canvas);
        }
    }

    public void drawSidebar() {
        float rockPos = (Rock.rockCount > 0) ? Rock.rocks[Rock.rockCount-1].getPosition().getY() : HEIGHT;

        paint.setColor(Util.HOGLINE_COLOR);
        canvas.drawRect(
                0,
                (1-0.193f)*HEIGHT - Util.HOGLINE_WEIGHT*WIDTH,
                Util.SIDEBAR_WIDTH*WIDTH,
                (1-0.193f)*HEIGHT + Util.HOGLINE_WEIGHT*WIDTH,paint);
        canvas.drawRect(
                0,
                (1-0.785f)*HEIGHT - Util.HOGLINE_WEIGHT*WIDTH,
                Util.SIDEBAR_WIDTH*WIDTH,
                (1-0.785f)*HEIGHT + Util.HOGLINE_WEIGHT*WIDTH,paint);

        paint.setColor(Color.argb(240,80,80,128));
        canvas.drawCircle(
                Util.SIDEBAR_WIDTH*WIDTH/2,
                HEIGHT + (rockPos-HEIGHT) * HEIGHT / (Util.SHEET_LENGTH + 4 * Util.HOUSE_RADIUS),
                Util.SIDEBAR_WIDTH*WIDTH/2,paint);

        int debugColor = 0;
        switch(state) {
            case Shooting:
                debugColor = Color.BLUE;
                break;
            case Sweeping:
                debugColor = Color.GREEN;
                break;
            case House:
                debugColor = Color.RED;
                break;
            case End:
                debugColor = Color.BLACK;
                break;
        }
        paint.setColor(debugColor);
        canvas.drawCircle(WIDTH - 20,HEIGHT/2,20,paint);
    }

    public void drawCounters() {
        int c1 = 0;
        int c2 = 1;
        int counterColor1 = Color.argb(100,Color.red(colors[c1]),Color.green(colors[c1]),Color.blue(colors[c1]));
        int counterColor2 = Color.argb(100,Color.red(colors[c2]),Color.green(colors[c2]),Color.blue(colors[c2]));
        if(Rock.rockCount == 0) return;
        for(int c = Rock.rockCount - 1; c < 16; c++) {
            if(c % 2 == 0) {
                paint.setColor(counterColor2);
                canvas.drawCircle(
                        Util.COUNTER_POS * WIDTH,
                        HEIGHT-Util.COUNTER_RADIUS*Util.ROCK_RADIUS*2*(7.5f-(c/2)),
                        Util.COUNTER_RADIUS*Util.ROCK_RADIUS,
                        paint);
            }
            else {
                paint.setColor(counterColor1);
                canvas.drawCircle(
                        WIDTH - Util.COUNTER_POS * WIDTH,
                        HEIGHT-Util.COUNTER_RADIUS*Util.ROCK_RADIUS*2*(7.5f-(c/2)),
                        Util.COUNTER_RADIUS*Util.ROCK_RADIUS,
                        paint);
            }
        }
    }

    public void drawHouse() {
        paint.setColor(Color.BLUE);
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y - yOffset,Util.HOUSE_RADIUS,paint);
        paint.setColor(Color.argb(255,250,250,255));
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y - yOffset,Util.HOUSE_RADIUS - Util.ANNULUS_WIDTH,paint);
        paint.setColor(Color.argb(255,255,0,0));
        canvas.drawCircle(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y - yOffset,Util.HOUSE_RADIUS - 2*Util.ANNULUS_WIDTH,paint);
        paint.setColor(Color.argb(255,250,250,255));
        canvas.drawCircle(WIDTH/2,Util.HOUSE_CENTER_Y - yOffset,Util.ROCK_RADIUS,paint);

        //Draw horizontal line through the button
        paint.setColor(Util.LINE_COLOR);
        canvas.drawRect(
                0,
                Util.HOUSE_CENTER_Y - yOffset + WIDTH * Util.CENTER_LINE_WEIGHT/2,
                WIDTH,
                Util.HOUSE_CENTER_Y - yOffset - WIDTH * Util.CENTER_LINE_WEIGHT/2,paint);
    }

    public void drawHogLines() {
        paint.setColor(Util.HOGLINE_COLOR);
        canvas.drawRect(
                0,
                (float)(HEIGHT - 4.5 * Util.HOUSE_RADIUS - yOffset - WIDTH * Util.HOGLINE_WEIGHT/2),
                WIDTH,
                (float)(HEIGHT - 4.5 * Util.HOUSE_RADIUS - yOffset + WIDTH * Util.HOGLINE_WEIGHT/2),
                paint
        );
        canvas.drawRect(
                0,
                (float)(Util.HOUSE_CENTER_Y + 3.5 * Util.HOUSE_RADIUS - yOffset - WIDTH * Util.HOGLINE_WEIGHT/2),
                WIDTH,
                (float)(Util.HOUSE_CENTER_Y + 3.5 * Util.HOUSE_RADIUS - yOffset + WIDTH * Util.HOGLINE_WEIGHT/2),
                paint
        );
        canvas.drawLine(
                0,
                Util.HOUSE_CENTER_Y - Util.HOUSE_RADIUS - Util.HOUSE_RADIUS/3 - yOffset,
                WIDTH,
                Util.HOUSE_CENTER_Y - Util.HOUSE_RADIUS - Util.HOUSE_RADIUS/3 - yOffset,
                paint);
    }

    public void drawSlider() {
        paint.setColor(Color.argb(180,0,0,0));
        canvas.drawRect(0,0,WIDTH,HEIGHT * Util.SLIDER_BACKGROUND,paint);

        paint.setColor(Color.argb(180,0xFF,0xFF,0xFF));
        canvas.drawRect(
                WIDTH * (1.0f - Util.SLIDER_LENGTH),
                HEIGHT * (Util.SLIDER_BACKGROUND)/2.0f,
                WIDTH * Util.SLIDER_LENGTH,
                HEIGHT * (Util.SLIDER_BACKGROUND)/2.0f + 16.0f,
                paint);

        paint.setColor(Color.RED);
        canvas.drawCircle(sliderPos + WIDTH/2,HEIGHT * (Util.SLIDER_BACKGROUND)/2 + 8.0f,32.0f,paint);

        /*
        paint.setTextSize(Util.SLIDER_BACKGROUND * HEIGHT / 8.0f);
        paint.setColor(Color.argb(180,255,255,255));
        paint.setTextAlign(Paint.Align.LEFT);
        canvas.drawText("Counterclockwise",0,HEIGHT * Util.SLIDER_BACKGROUND,paint);
        paint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText("Clockwise",WIDTH,HEIGHT * Util.SLIDER_BACKGROUND,paint);
        */
    }

    public void drawScoreSheet() {
        int fontSize = (int)(WIDTH/12.0f);
        int height = 3 * fontSize + 30;
        paint.setColor(Color.BLACK);
        paint.setAlpha(150);
        canvas.drawRect(
                0,
                HEIGHT - height,
                WIDTH,
                HEIGHT,
                paint
        );
        paint.setColor(Color.WHITE);
        paint.setAlpha(0xFF);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(fontSize);
        String line1 = "", line2_1 = "", line2_2 = "", line3 = "";
        for(int c = 0; c < 10; c++) {
            if(c < Util.NUM_ENDS) {
                line2_1 += "" + (c+1) + "\t";
                if(c <= currentEnd) {
                    line1 += "" + scores[0][c] + "\t";
                    line3 += "" + scores[1][c] + "\t";
                }else {
                    line1 += " \t";
                    line3 += " \t";
                }
            }
            //else line2_2 += "" + (c+1) + "\t";
        }
        canvas.drawText(line1,0,HEIGHT - height + fontSize,paint);
        paint.setFakeBoldText(true);
        paint.setColor(Color.rgb(0x88,0x88,0x88));
        canvas.drawText(line2_1 + line2_2,0,HEIGHT - height + 2 * fontSize + 10,paint);
        paint.setColor(Color.WHITE);
        paint.setFakeBoldText(false);
        canvas.drawText(line3,0,HEIGHT - height + 3 * fontSize + 20,paint);
    }

    public boolean noMotion() {
        for (int c = 0; c < Rock.rockCount; c++) {
            if(Rock.rocks[c].getVelocity().getMagnitude() != 0 && !Rock.rocks[c].isOutOfBounds()) return false;
        }
        return true;
    }

    public int checkWinner() {
        //Sort in increasing order of distance from the button
        for(int c = 0; c < 16; c++) {
            for(int j = c + 1; j < 16; j++) {
                float distC = (Rock.rocks[c].isOutOfBounds()) ? 1000000f : Util.euclideanDistance(Rock.rocks[c].getPosition(),new Vector2f(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y));
                if(distC > Util.HOUSE_RADIUS + Util.ROCK_RADIUS) Rock.rocks[c].flagOB();
                float distJ = (Rock.rocks[j].isOutOfBounds()) ? 1000000f : Util.euclideanDistance(Rock.rocks[j].getPosition(),new Vector2f(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y));
                if(distJ > Util.HOUSE_RADIUS + Util.ROCK_RADIUS) Rock.rocks[j].flagOB();
                if(distC >= distJ) {
                    Rock tmp = Rock.rocks[j];
                    Rock.rocks[j] = Rock.rocks[c];
                    Rock.rocks[c] = tmp;
                }
            }
        }

        if(Rock.rocks[0].isOutOfBounds()) return 0;
        int points = 0;
        int winner = Rock.rocks[0].getColor();
        for(int c = 0; c < 16; c++) {
            if(Rock.rocks[c].isOutOfBounds() || Util.euclideanDistance(Rock.rocks[c].getPosition(),new Vector2f(Util.HOUSE_CENTER_X,Util.HOUSE_CENTER_Y)) > Util.HOUSE_RADIUS + Util.ROCK_RADIUS)
                break;
            if(Rock.rocks[c].getColor() != winner) {
                for(int j = c; j < 16; j++) {
                    Rock.rocks[j].setOpacity(128);
                }
                break;
            }
            points++;
        }

        if(winner == colors[1]) return points;
        return -points;
    }

    public void control() {
        try {
            gameThread.sleep(PERIOD);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getScreenWidth() {
        return WIDTH;
    }

    public int getScreenHeight() {
        return HEIGHT;
    }
}
